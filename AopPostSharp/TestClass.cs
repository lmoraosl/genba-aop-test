﻿using System;

namespace AopPostSharp
{
    internal class TestClass
    {
        [LogMethod]
        public void Do(int a, string b)
        {
            Console.WriteLine($"Do Something {a} {b}");
        }


        [LogMethod]
        public void DoException(int a, string b)
        {
            throw new Exception("DoException");
        }
    }
}