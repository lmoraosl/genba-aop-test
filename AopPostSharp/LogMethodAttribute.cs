﻿using System;
using PostSharp.Aspects;

namespace AopPostSharp
{
    [Serializable]
    public sealed class LogMethodAttribute : OnMethodBoundaryAspect
    {
        public override void OnEntry(MethodExecutionArgs args)
        {
            if (args != null && args.Method != null)
            {
                var parameters = args.Method.GetParameters();

                var i = 0;
                foreach (var parameter in parameters)
                {
                    Console.WriteLine($"{parameter.Name} {args.Arguments[i]}");

                    i++;
                }

                Console.WriteLine("Entered");
            }
        }

        public override void OnSuccess(MethodExecutionArgs args)
        {
            if (args != null && args.Method != null)
            {

            }
        }

        public override void OnException(MethodExecutionArgs args)
        {
            if (args != null && args.Method != null)
            {

            }
        }
    }
}