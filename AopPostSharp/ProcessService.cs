﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;

namespace AopPostSharp
{
    internal class ProcessService : BackgroundService
    {
        private readonly TestClass _testClass;

        public ProcessService()
        {
            _testClass = new TestClass();
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            stoppingToken.Register(() =>
            {

            });

            while (!stoppingToken.IsCancellationRequested)
            {
                _testClass.Do(1, "Hello");

                await Task.Delay(TimeSpan.FromMilliseconds(2000), stoppingToken);
            }
        }

        public override void Dispose()
        {

        }
    }
}