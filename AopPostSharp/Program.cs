﻿using System;
using System.Runtime.Serialization;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using AopPostSharp;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using PostSharp.Serialization;

namespace AopPostSharp
{
    class Program
    {
        public static async Task<int> Main(string[] args)
        {
            try
            {
                var host = Host.CreateDefaultBuilder(args)
                    .ConfigureServices((hostContext, services) =>
                    {
                        services.AddHostedService<ProcessService>();
                    })
                    .ConfigureLogging(logging =>
                    {

                    })
                    .Build();

                await host.RunAsync();

                return 0;
            }
            catch (Exception ex)
            {

                return 1;
            }
            finally
            {

            }
        }
    }
}
