﻿using Castle.DynamicProxy;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using SimpleProxy.Configuration;
using SimpleProxy.Internal;

namespace AopAllIn
{
    static class SimpleProxyExtensions
    {
        public static IServiceCollection AddTransientWithProxy(this IServiceCollection services, ServiceDescriptor descriptor)
        {
            var serviceProvider = services.BuildServiceProvider();
            var proxyConfiguration = services.GetProxyConfiguration();
            var proxyGenerator = serviceProvider.GetService<IProxyGenerator>();

            var proxyInstance = ActivatorUtilities.CreateInstance(serviceProvider, descriptor.ImplementationType);

            services.AddTransient(descriptor.ServiceType, p => new ProxyFactory(serviceProvider, proxyGenerator, proxyConfiguration).CreateProxy(descriptor.ServiceType, proxyInstance));

            return services;
        }

        private static SimpleProxyConfiguration GetProxyConfiguration(this IServiceCollection services)
        {
            return services.BuildServiceProvider().GetRequiredService<IOptions<SimpleProxyConfiguration>>().Value;
        }
    }
}