﻿using Castle.DynamicProxy;
using Microsoft.Extensions.DependencyInjection;

namespace AopAllIn
{
    static class CasteProxyExtensions
    {
        public static IServiceCollection AddTransientProxed(this IServiceCollection services, ServiceDescriptor descriptor)
        {
            var serviceProvider = services.BuildServiceProvider();
            var proxyGenerator = serviceProvider.GetService<IProxyGenerator>();
            var interceptorsFactory = serviceProvider.GetService<IInterceptorsFactory>();

            var proxyInstance = ActivatorUtilities.CreateInstance(serviceProvider, descriptor.ImplementationType);

            services.AddTransient(descriptor.ServiceType, p =>
            {
                var proxy = proxyGenerator.CreateInterfaceProxyWithTarget(descriptor.ServiceType, proxyInstance, interceptorsFactory.Create());

                return proxy;
            });

            return services;
        }
    }
}