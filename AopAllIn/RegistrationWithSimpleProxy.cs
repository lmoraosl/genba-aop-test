﻿using System;
using Castle.DynamicProxy;
using Microsoft.Extensions.DependencyInjection;
using Scrutor;

namespace AopAllIn
{
    sealed class RegistrationWithSimpleProxy : RegistrationStrategy
    {
        public override void Apply(IServiceCollection services, ServiceDescriptor descriptor)
        {
            switch (descriptor.Lifetime)
            {
                case ServiceLifetime.Singleton:
                    break;
                case ServiceLifetime.Scoped:
                    break;
                case ServiceLifetime.Transient:
                    services.AddTransientWithProxy(descriptor);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}