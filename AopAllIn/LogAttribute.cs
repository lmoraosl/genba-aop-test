﻿using Microsoft.Extensions.Logging;
using SimpleProxy.Attributes;

namespace AopSimpleProxy
{
    public class LogAttribute : MethodInterceptionAttribute
    {
        internal LogLevel LoggingLevel { get; set; }

        public LogAttribute(LogLevel loggingLevel)
        {
            LoggingLevel = loggingLevel;
        }
    }
}