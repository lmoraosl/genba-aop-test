﻿using System;
using System.Linq;
using Castle.DynamicProxy;
using Microsoft.Extensions.Logging;
using SimpleProxy;
using SimpleProxy.Interfaces;
using SimpleProxy.Internal.Extensions;

namespace AopSimpleProxy
{
    public class LogInterceptor : IInterceptor
    {
        private readonly ILoggerFactory _loggerFactory;
        private ILogger _logger;

        public LogInterceptor(ILoggerFactory loggerFactory)
        {
            _loggerFactory = loggerFactory;
        }
        
        public void Intercept(IInvocation invocation)
        {
            _logger = _loggerFactory.CreateLogger(invocation.TargetType);

            _logger.LogInformation($"Before {invocation.TargetType}{invocation.Method.Name}");

            invocation.Proceed();

            _logger.LogInformation($"After {invocation.TargetType}{invocation.Method.Name}");
        }
    }
}