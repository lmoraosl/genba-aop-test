﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;

namespace AopAllIn
{
    internal class ProcessService : BackgroundService
    {
        private readonly ITestClass1 _testClass1;
        private readonly ITestClass2 _testClass2;

        public ProcessService(ITestClass1 testClass1, ITestClass2 testClass2)
        {
            _testClass1 = testClass1;
            _testClass2 = testClass2;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            stoppingToken.Register(() =>
            {

            });

            while (!stoppingToken.IsCancellationRequested)
            {
                _testClass1.Do();
                _testClass2.Do();

                await Task.Delay(TimeSpan.FromMilliseconds(2000), stoppingToken);
            }
        }

        public override void Dispose()
        {

        }
    }
}