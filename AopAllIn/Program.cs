﻿using System;
using System.Threading.Tasks;
using Castle.DynamicProxy;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace AopAllIn
{
    class Program
    {
        public static async Task<int> Main(string[] args)
        {
            try
            {
                var host = Host
                    .CreateDefaultBuilder(args)
                    .ConfigureServices((hostContext, services) =>
                    {
                        services.AddOptions();
                        services.AddLogging(p => p.AddConsole(x => x.IncludeScopes = true).SetMinimumLevel(LogLevel.Trace));

                        services.AddSingleton<IProxyGenerator, ProxyGenerator>();
                        services.AddSingleton<IInterceptorsFactory, InterceptorsFactory>();

                        services.Scan(
                            scan =>
                                scan
                                    .FromCallingAssembly()
                                    .AddClasses(classes => classes.Where(x => x.Namespace.StartsWith("AopAllIn")))
                                    .AsMatchingInterface()
                                    .UsingRegistrationStrategy(new RegistrationWithCastleProxy()));

                        services.AddHostedService<ProcessService>();
                    })
                    .ConfigureLogging(logging =>
                    {

                    })
                    .Build();

                await host.RunAsync();

                return 0;
            }
            catch (Exception ex)
            {

                return 1;
            }
            finally
            {

            }
        }
    }
}
