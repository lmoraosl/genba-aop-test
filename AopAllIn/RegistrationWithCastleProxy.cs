﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Scrutor;

namespace AopAllIn
{
    sealed class RegistrationWithCastleProxy : RegistrationStrategy, IRegistrationWithCastleProxy
    {
        public RegistrationWithCastleProxy()
        {

        }

        public override void Apply(IServiceCollection services, ServiceDescriptor descriptor)
        {
            switch (descriptor.Lifetime)
            {
                case ServiceLifetime.Singleton:
                    break;
                case ServiceLifetime.Scoped:
                    break;
                case ServiceLifetime.Transient:
                    services.AddTransientProxed(descriptor);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}