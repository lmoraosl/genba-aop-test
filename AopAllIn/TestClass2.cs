﻿using System;
using AopSimpleProxy;
using Microsoft.Extensions.Logging;

namespace AopAllIn
{
    [Proxable]
    class TestClass2 : ITestClass2
    {
        private readonly ILogger _logger;

        public TestClass2(ILogger<TestClass2> logger)
        {
            _logger = logger;
        }

        [Log(LogLevel.Information)]
        public void Do()
        {
            _logger.LogInformation("Do2");
        }
    }
}