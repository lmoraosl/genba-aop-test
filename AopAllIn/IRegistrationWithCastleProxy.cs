﻿using Microsoft.Extensions.DependencyInjection;

namespace AopAllIn
{
    interface IRegistrationWithCastleProxy
    {
        void Apply(IServiceCollection services, ServiceDescriptor descriptor);
    }
}