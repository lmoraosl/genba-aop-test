﻿using Castle.DynamicProxy;

namespace AopAllIn
{
    public interface IInterceptorsFactory
    {
        IInterceptor[] Create();
    }
}