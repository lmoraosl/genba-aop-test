﻿using System;
using AopSimpleProxy;
using Microsoft.Extensions.Logging;

namespace AopAllIn
{
    [Proxable]
    public class TestClass1 : ITestClass1
    {
        private readonly ILogger _logger;

        public TestClass1(ILogger<TestClass1> logger)
        {
            _logger = logger;
        }

        [Log(LogLevel.Information)]
        public void Do()
        {
            _logger.LogInformation("Do1");
        }
    }
}