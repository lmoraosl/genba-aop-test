﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace AopAllIn
{
    [AttributeUsage(AttributeTargets.Class)]
    [ExcludeFromCodeCoverage]
    class ProxableAttribute : Attribute
    {

    }
}