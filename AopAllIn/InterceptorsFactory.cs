﻿using AopSimpleProxy;
using Castle.DynamicProxy;
using Microsoft.Extensions.Logging;

namespace AopAllIn
{
    public class InterceptorsFactory : IInterceptorsFactory
    {
        private readonly ILoggerFactory _loggerFactory;

        public InterceptorsFactory(ILoggerFactory loggerFactory)
        {
            _loggerFactory = loggerFactory;
        }
        public IInterceptor[] Create()
        {
            return new IInterceptor[] {new LogInterceptor(_loggerFactory) };
        }
    }
}