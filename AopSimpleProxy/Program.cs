﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using SimpleProxy.Extensions;
using SimpleProxy.Strategies;


namespace AopSimpleProxy
{
    class Program
    {
        public static async Task<int> Main(string[] args)
        {
            try
            {
                var host = Host.CreateDefaultBuilder(args)
                    .ConfigureServices((hostContext, services) =>
                    {
                        services.AddOptions();
                        services.AddLogging(p => p.AddConsole(x => x.IncludeScopes = true).SetMinimumLevel(LogLevel.Trace));

                        services.EnableSimpleProxy(p => p
                            .AddInterceptor<LogAttribute, LogInterceptor>()
                            .WithOrderingStrategy<PyramidOrderStrategy>());

                        services.AddTransientWithProxy<ITestClass, TestClass>();

                        services.AddHostedService<ProcessService>();
                    })
                    .ConfigureLogging(logging =>
                    {

                    })
                    .Build();

                await host.RunAsync();

                return 0;
            }
            catch (Exception ex)
            {

                return 1;
            }
            finally
            {

            }
        }
    }
}
