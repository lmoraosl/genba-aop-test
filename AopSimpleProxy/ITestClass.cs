﻿namespace AopSimpleProxy
{
    public interface ITestClass
    {
        void Do(int a, string b);
    }
}