﻿using Microsoft.Extensions.Logging;

namespace AopSimpleProxy
{
    class TestClass : ITestClass
    {
        private readonly ILogger<TestClass> _logger;

        public TestClass(ILogger<TestClass> logger)
        {
            _logger = logger;
        }

        [Log(LogLevel.Information)]
        public void Do(int a, string b)
        {
            _logger.LogInformation("Hello");
        }
    }
}