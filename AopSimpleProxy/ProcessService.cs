﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;

namespace AopSimpleProxy
{
    internal class ProcessService : BackgroundService
    {
        private readonly ITestClass _testClass;

        public ProcessService(ITestClass testClass)
        {
            _testClass = testClass;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            stoppingToken.Register(() =>
            {

            });

            while (!stoppingToken.IsCancellationRequested)
            {
                _testClass.Do(1, "aaa");

                await Task.Delay(TimeSpan.FromMilliseconds(2000), stoppingToken);
            }
        }

        public override void Dispose()
        {

        }
    }
}