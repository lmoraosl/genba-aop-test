﻿using Microsoft.Extensions.Logging;
using SimpleProxy;
using SimpleProxy.Interfaces;

namespace AopSimpleProxy
{
    public class LogInterceptor : IMethodInterceptor
    {
        private readonly ILoggerFactory _loggerFactory;
        private LogAttribute _loggingAttribute;
        private ILogger _logger;

        public LogInterceptor(ILoggerFactory loggerFactory)
        {
            _loggerFactory = loggerFactory;
        }

        public void BeforeInvoke(InvocationContext invocationContext)
        {
            _logger = _loggerFactory.CreateLogger(invocationContext.GetOwningType());

            _loggingAttribute = invocationContext.GetAttributeFromMethod<LogAttribute>();

            var loggingLevel = _loggingAttribute.LoggingLevel;

            _logger.Log(loggingLevel, $"{invocationContext.GetOwningType()}: Method executing: {invocationContext.GetExecutingMethodName()}");
        }

        public void AfterInvoke(InvocationContext invocationContext, object methodResult)
        {
            _logger.Log(_loggingAttribute.LoggingLevel, $"{invocationContext.GetOwningType()}: Method executed: {invocationContext.GetExecutingMethodName()}");
        }
    }
}