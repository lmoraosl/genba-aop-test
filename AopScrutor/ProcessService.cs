﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;

namespace AopScrutor
{
    internal class ProcessService : BackgroundService
    {
        private readonly IMyService _myserice;

        public ProcessService(IMyService myserice)
        {
            _myserice = myserice;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            stoppingToken.Register(() =>
            {

            });

            while (!stoppingToken.IsCancellationRequested)
            {
                _myserice.Hello();

                await Task.Delay(TimeSpan.FromMilliseconds(2000), stoppingToken);
            }
        }

        public override void Dispose()
        {
            
        }
    }
}