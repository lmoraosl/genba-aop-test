﻿using System;

namespace AopScrutor
{
    internal class MyServiceDecorated : IMyService
    {
        private readonly IMyService _myService;

        public MyServiceDecorated(IMyService myService)
        {
            _myService = myService;
        }

        public void Hello()
        {
            Console.WriteLine("Before Hello");

            _myService.Hello();

            Console.WriteLine("After Hello");
        }
    }
}