﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace AopScrutor
{
    class Program
    {
        public static async Task<int> Main(string[] args)
        {
            try
            {
                var host = Host.CreateDefaultBuilder(args)
                    .ConfigureServices((hostContext, services) =>
                    {
                        services.AddHostedService<ProcessService>();

                        services.AddScoped<IMyService, MyService>();

                        services.Decorate<IMyService, MyServiceDecorated>();
                    })
                    .ConfigureLogging(logging =>
                    {

                    })
                    .Build();

                await host.RunAsync();

                return 0;
            }
            catch (Exception ex)
            {

                return 1;
            }
            finally
            {

            }
        }
    }
}
