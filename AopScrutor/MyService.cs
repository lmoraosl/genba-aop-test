﻿using System;

namespace AopScrutor
{
    internal class MyService : IMyService
    {
        public void Hello()
        {
            Console.WriteLine("Hello");
        }
    }
}