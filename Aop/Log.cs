﻿using AspectInjector.Broker;

using Serilog.Events;

using System;

namespace Aop
{
    [Injection(typeof(LogAspect))]
    public sealed class Log : Attribute
    {
        public LogEventLevel Level { get; }
        public Log(LogEventLevel level)
        {
            Level = level;
        }
    }
}