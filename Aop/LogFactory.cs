﻿using Serilog.Core;

using System;

namespace Aop
{
    public static class LogFactory
    {
        public static Logger Log { get; set; }
        public static object GetInstance(Type type)
        {
            return new LogAspect(Log);
        }
    }
}