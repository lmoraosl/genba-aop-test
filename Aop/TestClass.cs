﻿using Serilog.Events;

using System;

namespace Aop
{
    internal class TestClass
    {
        [Log(LogEventLevel.Information)]
        public void Do(int a, string b)
        {
            Console.WriteLine("Do Something");
        }
    }
}