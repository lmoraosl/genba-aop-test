﻿using Serilog;

namespace Aop
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var log = new LoggerConfiguration()
              .WriteTo.Console()
              .CreateLogger();

            //La libreria supporta solo una classe statica come factory unico modo è iniettare la dipendenza
            LogFactory.Log = log;

            var testClass1 = new TestClass();

            testClass1.Do(1, "primo");

            var testClass2 = new TestClass();

            testClass2.Do(2, "secondo");
        }
    }
}