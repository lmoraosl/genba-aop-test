﻿using AspectInjector.Broker;

using Newtonsoft.Json;

using Serilog.Core;
using Serilog.Events;

using System;

namespace Aop
{
    [Aspect(Scope.PerInstance, Factory = typeof(LogFactory))]
    public class LogAspect
    {
        private readonly Logger _log;

        public LogAspect(Logger log)
        {
            _log = log;
        }

        [Advice(Kind.Before, Targets = Target.Method)]
        public void OnBeforeEntry(
          [Argument(Source.Instance)] object instance,
          [Argument(Source.Triggers)] Attribute[] triggers,
          [Argument(Source.Type)] Type type,
          [Argument(Source.Name)] string name,
          [Argument(Source.Arguments)] object[] args)
        {
            foreach (var trigger in triggers)
            {
                if (trigger is Log log)
                {
                    if (log.Level == LogEventLevel.Information)
                        _log.Information($"Entering method {type} {name} {JsonConvert.SerializeObject(args)}");
                }
            }
        }

        [Advice(Kind.After, Targets = Target.Method | Target.Any)]
        public void OnAfterEntry(
          [Argument(Source.Type)] Type type,
          [Argument(Source.Name)] string name,
          [Argument(Source.Arguments)] object[] args)
        {
            _log.Information($"Exiting method {type} {name}.");
        }
    }
}